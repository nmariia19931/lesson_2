package com.mycompany.l02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTests extends TestBase {

    @Test
    public void searchTheItemAssert() {
        WebElement searchField = driver.findElement(By.name("search"));
        typeText(searchField, "141007098");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        waitForPageLoaded();
        assertTrue(isElementPresent(By.xpath("//strong[contains(text(),'141007098')]")));
        driver.findElement(By.xpath("//a[@href='/context/detail/id/141007098/'][2]")).click();
        waitForPageLoaded();
        assertTrue(isElementPresent(By.xpath("//div[@data-widget='webProductHeading'']")));
        assertEquals(driver.findElement(By.xpath("//div[@data-widget='webSale']//div[2]/span[1]"))
                .getText(), "378");
        assertTrue(driver.findElement(By.xpath("//div[@data-widget='webSale']//div[2]/span[1]")).getCssValue("color")
                .contains("rgba(249, 17, 85, 1)"));
        assertEquals(driver.findElement(By.xpath("(//div[@data-widget='webCharacteristics']//span[text()='Бренд']//ancestor::dl[1]//dd)[1]"))
                .getText(), "Faber-Castell");
    }

    @Test
    public void searchTheItemSoftAssert() {
        WebElement searchField = driver.findElement(By.name("search"));
        typeText(searchField, "141007098");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        waitForPageLoaded();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(isElementPresent(By.xpath("//strong[contains(text(),'141007098')]")));
        driver.findElement(By.xpath("//a[@href='/context/detail/id/141007098/'][2]")).click();
        waitForPageLoaded();
        softAssert.assertTrue(isElementPresent(By.xpath("//div[@data-widget='webProductHeading'']")));
        softAssert.assertEquals(driver.findElement(By.xpath("//div[@data-widget='webSale']//div[2]/span[1]")).getText(),
                "378");
        softAssert.assertEquals(driver.findElement(By.xpath("//div[@data-widget='webSale']//div[2]/span[1]")).getCssValue("color"),
                "rgba(249, 17, 85, 1)");
        softAssert.assertEquals(driver.findElement(By.xpath("(//div[@data-widget='webCharacteristics']//span[text()='Бренд']//ancestor::dl[1]//dd)[1]"))
                .getText(), "Faber-Castell");
    }

    public void typeText(WebElement element, String text) {
        element.click();
        element.clear();
        element.sendKeys(text);

    }
}
